# Beh Kamenkou

Timer for [Beh Kamenkou](https://duckbar.cz/behy.html) based on the original [behKamenkou](https://github.com/Element104/behKamenkou) implemented in QT.

# screenshots

## before start

![Before Start](screenshots/before-start.png "Before Start")

## during race

![During Race](screenshots/during-race.png "During Race")
