use std::fmt;
use std::fs::File;
use std::io::prelude::*;
use std::io::{Error, ErrorKind};
use std::time::{Duration, SystemTime, UNIX_EPOCH};

use cursive::views::{EditView, TextView};
use cursive::Cursive;

use super::registration;

#[derive(Debug)]
pub struct ResultEntry {
    pub rank: u32,
    pub time: Duration,
    pub number: Option<String>,
}

impl ResultEntry {
    pub fn new(rank: u32, time: Duration, number: Option<String>) -> Self {
        ResultEntry { rank, time, number }
    }
}

impl fmt::Display for ResultEntry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let number = match &self.number {
            Some(number) => String::from(number),
            None => String::from(""),
        };
        let secs = self.time.as_secs();

        write!(f, "{};{};{}", self.rank, secs.to_string(), number)
    }
}

#[derive(Debug)]
pub struct Race {
    pub started: Option<SystemTime>,
    pub results: Vec<ResultEntry>,
    pub last_rank: u32,
    pub registration: registration::Registration,
}

impl fmt::Display for Race {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.started {
            Some(started) => {
                let secs = started.elapsed().unwrap().as_secs();
                let h: u64 = secs / 3600;
                let m: u64 = (secs - (h * 3600)) / 60;
                let s: u64 = secs - (h * 3600) - (m * 60);
                write!(f, "{:0>2}:{:0>2}:{:0>2}", h, m, s)
            }
            None => write!(f, "00:00:00"),
        }
    }
}

pub enum RaceEvent {
    Start,
    AddTime,
    Edit,
    Save,
    Quit,
    GenerateResults,
}

impl Race {
    pub fn new() -> Self {
        let mut race = Race {
            started: None,
            results: Vec::new(),
            last_rank: 0,
            registration: registration::Registration::new(),
        };
        registration::load_registration(&mut race.registration).unwrap();
        race
    }

    pub fn start(&mut self) {
        if self.started.is_none() {
            self.started = Some(SystemTime::now());
        }
    }

    pub fn add_result(&mut self) -> Option<(u32, String)> {
        match self.started {
            Some(started) => {
                let time = started.elapsed().unwrap();
                self.last_rank += 1;
                self.results
                    .push(ResultEntry::new(self.last_rank, time, None));
                Some((self.last_rank, self.to_string()))
            }
            None => None,
        }
    }

    pub fn save(&mut self) -> std::io::Result<()> {
        let mut file = File::create("race.csv")?;
        if let Some(started) = self.started {
            writeln!(
                &mut file,
                "{}",
                started.duration_since(UNIX_EPOCH).unwrap().as_secs()
            )
            .unwrap();
        }

        self.results.iter().fold(1, |rank: i32, result| {
            writeln!(&mut file, "{}", result)
                .or_else(|_| Err(()))
                .unwrap();
            rank + 1
        });

        self.registration = registration::Registration::new();
        registration::load_registration(&mut self.registration).unwrap();
        self.save_categories_as_csv().unwrap();
        Ok(())
    }

    fn save_categories_as_csv(&self) -> std::io::Result<()> {
        let raceresults = super::make_registration_with_results(&self.results, &self.registration);
        let categories = super::get_categories(&self.registration);
        for category in categories {
            let mut file = File::create(format!("vysledky-{}.csv", category))?;
            let cat_results = super::get_category_results(category.as_str(), &raceresults);
            cat_results.iter().fold(1, |rank: u32, raceresult| {
                if raceresult.l_name != "" {
                    writeln!(
                        &mut file,
                        "{};{};{};{};{};{};{}",
                        rank,
                        raceresult.number,
                        raceresult.l_name,
                        raceresult.f_name,
                        raceresult.year,
                        raceresult.sex,
                        raceresult.time,
                    )
                        .or_else(|_| Err(()))
                        .unwrap();
                } else {
                    writeln!(
                        &mut file,
                        "{};{};{};{};{};{}",
                        raceresult.number,
                        raceresult.f_name,
                        raceresult.year,
                        raceresult.team,
                        raceresult.time,
                        rank,
                    )
                        .or_else(|_| Err(()))
                        .unwrap();
                }

                rank + 1
            });
        }
        Ok(())
    }

    pub fn restore(&mut self, filename: &str) -> std::io::Result<()> {
        let mut file = File::open(filename)?;
        let mut race_data = String::new();
        file.read_to_string(&mut race_data)?;
        let mut race_data_lines = race_data.as_str().lines();

        // let duration = Duration::from_secs(race_data_lines.next().unwrap().parse().unwrap());

        // self.started = Some(UNIX_EPOCH + duration);

        self.started = match race_data_lines.next() {
            Some(duration_str) => {
                let duration = Duration::from_secs(duration_str.parse().unwrap());
                Some(UNIX_EPOCH + duration)
            }
            None => return std::io::Result::Err(Error::new(ErrorKind::Other, "oh no!")),
        };

        let mut rank: u32 = 0;
        for race_data_line in race_data_lines {
            let mut r_t_n = race_data_line.split(';');
            let _rank: u32 = r_t_n.next().unwrap().parse().unwrap();
            rank += 1;
            let time = Duration::from_secs(r_t_n.next().unwrap().parse().unwrap());
            let number = match r_t_n.next() {
                Some(number) => {
                    if number == "" {
                        Some(String::from(""))
                    } else {
                        Some(String::from(number))
                    }
                }
                None => None,
            };
            let result = ResultEntry::new(rank, time, number);
            self.results.push(result);
            self.last_rank = rank;
        }
        Ok(())
    }

    pub fn update(&mut self, rank: u32, number: &str) {
        for result in &mut self.results {
            if result.rank == rank {
                result.number = Some(number.to_string());
            }
        }
    }

    pub fn update_from_screen(&mut self, siv: &mut Cursive) {
        for result in &mut self.results {
            result.number = Some(
                siv.find_name::<EditView>(&result.rank.to_string())
                    .unwrap()
                    .get_content()
                    .to_string(),
            );
        }
    }

    pub fn update_screen_from_race(&self, siv: &mut Cursive) {
        for result in &self.results {
            if let Some(number) = &result.number {
                if let Some(racer) = self.registration.get(number) {
                    let name = format!("{} {}", &racer.f_name, &racer.l_name);
                    siv.find_name::<TextView>(format!("{}-name", &result.rank).as_str())
                        .unwrap()
                        .set_content(name);
                }
            }
        }
    }
}
