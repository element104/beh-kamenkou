use std::thread::sleep;
use std::time::Duration;

use cursive::traits::*;
use cursive::view::{Offset, Position};
use cursive::views::{
    Button, Dialog, EditView, LinearLayout, ListView, PaddedView, ScrollView, TextView,
};

use beh_kamenkou;
use beh_kamenkou::race::{Race, RaceEvent};

fn main() {
    let mut r = Race::new();

    let mut siv = cursive::default();

//    match siv.load_toml("toml.theme") {
//        Err(_) => siv.load_toml("../theme.toml").unwrap(),
//        Ok(_) => {}
//    }

    siv.add_global_callback('q', |s| s.set_user_data(RaceEvent::Quit));
    siv.add_global_callback(' ', |s| s.set_user_data(RaceEvent::AddTime));
    siv.add_global_callback('s', |s| s.set_user_data(RaceEvent::Save));
    siv.add_global_callback('e', |s| s.set_user_data(RaceEvent::Edit));

    let mut main = LinearLayout::horizontal();

    let timer = TextView::new(r.to_string()).center().with_name("timer");

    let results = ListView::new()
        .child(
            "Rank",
            LinearLayout::horizontal()
                .child(TextView::new("Time    "))
                .child(TextView::new("  "))
                .child(TextView::new("Number"))
                .child(TextView::new("       "))
                .child(TextView::new("   Name     ")),
        )
        .with_name("results");

    let scroll =
        ScrollView::new(results).scroll_strategy(cursive::view::ScrollStrategy::StickToBottom);
    let linear = LinearLayout::vertical().child(scroll);

    main.add_child(PaddedView::lrtb(3, 3, 0, 0, linear));
    main.add_child(
        LinearLayout::vertical()
            .child(Button::new("Save", |s| s.set_user_data(RaceEvent::Save)))
            .child(Button::new("Add Time", |s| {
                s.set_user_data(RaceEvent::AddTime)
            })),
    );

    let enable_start = match r.restore("race.csv") {
        Ok(_) => false,
        _ => true,
    };

    let mut start_button = Button::new("Start", |s| s.set_user_data(RaceEvent::Start));
    start_button.set_enabled(enable_start);

    let controls = PaddedView::lrtb(
        1, 1, 0, 0,
        LinearLayout::vertical()
            .child(start_button)
            .child(Button::new("Add Time", |s| {
                s.set_user_data(RaceEvent::AddTime)
            }))
            .child(Button::new("Edit Results", |s| {
                s.set_user_data(RaceEvent::Edit)
            }))
            .child(TextView::new("   "))
            .child(TextView::new("Shortcuts"))
            .child(
                ListView::new()
                    .child("SPACE", TextView::new("Add time"))
                    .child("E", TextView::new("Edit results"))
                    //             .child("S", TextView::new("Save"))
                    .child("Q", TextView::new("Quit")),
            )
            .child(TextView::new("   "))
            .child(Button::new("Quit", |s| s.set_user_data(RaceEvent::Quit))),
    );

    siv.screen_mut().add_layer_at(
        Position::new(Offset::Parent(10), Offset::Parent(5)),
        Dialog::around(timer).title("Timer"),
    );

    siv.screen_mut().add_layer_at(
        Position::new(Offset::Absolute(30), Offset::Parent(0)),
        Dialog::around(main).title("Running results"),
    );

    siv.screen_mut().add_layer_at(
        Position::new(Offset::Absolute(110), Offset::Parent(0)),
        Dialog::around(controls).title("Controls"),
    );

    // restore results
    if enable_start == false {
        for result in &r.results {
            let secs = result.time.as_secs();
            let h: u64 = secs / 3600;
            let m: u64 = (secs - (h * 3600)) / 60;
            let s: u64 = secs - (h * 3600) - (m * 60);
            let time = format!("{:0>2}:{:0>2}:{:0>2}", h, m, s);

            siv.call_on_name("results", |view: &mut ListView| {
                let content = match &result.number {
                    Some(number) => number,
                    _ => "",
                };
                view.add_child(
                    &result.rank.to_string(),
                    LinearLayout::horizontal()
                        .child(TextView::new(time))
                        .child(TextView::new("  "))
                        .child(
                            // TODO: when result.number is not None, use the value
                            EditView::new()
                                .content(content)
                                .with_name(&result.rank.to_string())
                                .fixed_width(10),
                        )
                        .child(TextView::new("     "))
                        .child(
                            TextView::new("           ").with_name(format!("{}-name", &result.rank)),
                        ),
                );
            });
        }
        r.update_screen_from_race(&mut siv);
        // siv.refresh();
    }

    let wait_duration = Duration::from_millis(200);
    let mut wait_for_event: bool;

//    siv.run();
    let mut sivrunner = siv.runner();
    while sivrunner.is_running() {
        wait_for_event = true;
        loop {
            if sivrunner.step() {
                match sivrunner.take_user_data::<RaceEvent>() {
                    Some(RaceEvent::Start) => {
                        r.start();
                    }
                    Some(RaceEvent::AddTime) => {
                        if let Some((rank, time)) = r.add_result() {
                            sivrunner.call_on_name("results", |view: &mut ListView| {
                                view.add_child(
                                    &rank.to_string(),
                                    LinearLayout::horizontal()
                                        .child(TextView::new(&time))
                                        .child(TextView::new("  "))
                                        .child(
                                            EditView::new()
                                                .with_name(&rank.to_string())
                                                .fixed_width(10),
                                        )
                                        .child(TextView::new("     "))
                                        .child(
                                            TextView::new("           ")
                                                .with_name(format!("{}-name", &rank)),
                                        ),
                                );
                            });
                            r.update_from_screen(&mut sivrunner);
                            r.save().unwrap();
                        }
                    }
                    Some(RaceEvent::Quit) => {
                        r.update_from_screen(&mut sivrunner);
                        r.save().unwrap();
                        sivrunner.quit();
                    }
                    Some(RaceEvent::Save) => {
                        // Save data
                        r.update_from_screen(&mut sivrunner);
                        r.save().unwrap();
                        r.update_screen_from_race(&mut sivrunner);
                        // And move focus back to Controls panel
                        let results = sivrunner.pop_layer().unwrap();
                        let controls = sivrunner.pop_layer().unwrap();
                        sivrunner.screen_mut().add_layer_at(
                            Position::new(Offset::Absolute(30), Offset::Absolute(5)),
                            results,
                        );
                        sivrunner.screen_mut().add_layer_at(
                            Position::new(Offset::Absolute(110), Offset::Absolute(5)),
                            controls,
                        );
                        beh_kamenkou::save_results_as_html(&r.results).unwrap();
                    }
                    Some(RaceEvent::Edit) => {
                        // Move focus on Results dialog
                        let controls = sivrunner.pop_layer().unwrap();
                        let results = sivrunner.pop_layer().unwrap();
                        sivrunner.screen_mut().add_layer_at(
                            Position::new(Offset::Absolute(110), Offset::Absolute(5)),
                            controls,
                        );
                        sivrunner.screen_mut().add_layer_at(
                            Position::new(Offset::Absolute(30), Offset::Absolute(5)),
                            results,
                        );
                    }
                    Some(RaceEvent::GenerateResults) => {}
                    None => (),
                }
                wait_for_event = false;
            } else {
                break;
            }
        }

        let content = r.to_string();
        sivrunner.set_user_data(r.to_string());
        sivrunner.call_on_name("timer", move |view: &mut TextView| {
            view.set_content(content);
        });
        sivrunner.refresh();

        if wait_for_event {
            sleep(wait_duration);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_multiple_dialogs() {
        let mut siv = Cursive::default();

        siv.screen_mut().add_layer_at(
            Position::new(Offset::Parent(10), Offset::Parent(15)),
            Dialog::around(TextView::new("This is first dialog")).title("First"),
        );
        siv.screen_mut().add_layer_at(
            Position::new(Offset::Parent(40), Offset::Parent(0)),
            Dialog::around(TextView::new("Second dialog")).title("Second"),
        );

        siv.add_global_callback('q', |s| s.quit());
        siv.run();
    }

    #[test]
    fn test_focus() {
        let mut siv = Cursive::default();

        let l1 = LinearLayout::vertical()
            .child(TextView::new("This is first dialog"))
            .child(Button::new("Start", |s| s.focus_id("2").unwrap()).with_name("1"));

        siv.screen_mut().add_layer_at(
            Position::new(Offset::Absolute(10), Offset::Absolute(15)),
            l1,
        );

        let l2 = LinearLayout::vertical()
            .child(TextView::new("Second dialog"))
            .child(Button::new("Start", |s| s.focus_id("1").unwrap()))
            .with_name("2");

        siv.screen_mut().add_layer_at(
            Position::new(Offset::Absolute(50), Offset::Absolute(15)),
            l2,
        );

        // pub fn find_layer_from_id(&mut self, id: &str) -> Option<LayerPosition>
        // pub fn move_to_front(&mut self, layer: LayerPosition)

        siv.add_global_callback('q', |s| s.quit());
        siv.add_global_callback('1', |s| {
            let l1 = s.pop_layer().unwrap();
            let l2 = s.pop_layer().unwrap();
            s.screen_mut().add_layer_at(
                Position::new(Offset::Absolute(10), Offset::Absolute(15)),
                l1,
            );
            s.screen_mut()
                .add_layer_at(Position::new(Offset::Absolute(50), Offset::Parent(0)), l2);
        });
        siv.add_global_callback('2', |s| {
            let l1 = s.pop_layer().unwrap();
            let l2 = s.pop_layer().unwrap();
            s.screen_mut()
                .add_layer_at(Position::new(Offset::Absolute(50), Offset::Parent(0)), l1);
            s.screen_mut()
                .add_layer_at(Position::new(Offset::Parent(10), Offset::Parent(15)), l2);
        });
        //       siv.add_global_callback('2', |s| s.focus_id("2").unwrap());
        siv.run();
    }

    #[test]
    fn test_move_front() {
        let mut siv = Cursive::default();

        let l1 = LinearLayout::vertical()
            .child(TextView::new("This is first dialog"))
            .child(Button::new("Start", |s| s.focus_id("2").unwrap()).with_name("1"));

        siv.screen_mut()
            .add_layer_at(Position::new(Offset::Parent(10), Offset::Parent(15)), l1);

        let l2 = LinearLayout::vertical()
            .child(TextView::new("Second dialog"))
            .child(Button::new("Start", |s| s.focus_id("1").unwrap()))
            .with_name("2");

        siv.screen_mut()
            .add_layer_at(Position::new(Offset::Parent(40), Offset::Parent(0)), l2);

        // pub fn find_layer_from_id(&mut self, id: &str) -> Option<LayerPosition>
        // pub fn move_to_front(&mut self, layer: LayerPosition)

        siv.add_global_callback('q', |s| s.quit());
        siv.add_global_callback('1', |s| match s.screen_mut().find_layer_from_id("1") {
            Some(layer) => s.screen_mut().move_to_front(layer),
            _ => (),
        });
        siv.add_global_callback('2', |s| {
            match s.screen_mut().find_layer_from_id("2") {
                Some(layer) => s.screen_mut().move_to_front(layer),
                _ => (),
            }

            //            s.screen_mut().move_to_front(s.screen_mut().find_layer_from_id("2").unwrap());
        });
        //       siv.add_global_callback('2', |s| s.focus_id("2").unwrap());
        siv.run();
    }
}
