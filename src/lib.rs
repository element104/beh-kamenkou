pub mod race;
pub mod registration;

use glob::glob;

use race::ResultEntry;
use registration::Racer;

use std::fs::File;
use std::io::prelude::*;

use std::collections::HashMap;
use std::collections::HashSet;

use std::rc::Rc;

use serde_derive::Deserialize;

#[derive(Debug)]
pub struct RaceResult {
    pub time: String,
    pub number: String,
    pub f_name: String,
    pub l_name: String,
    pub year: String,
    pub sex: String,
    pub race_type: String,
    pub category: String,
    pub lap: u32,
    pub team: String,
}

#[derive(Deserialize, Debug)]
pub struct Category {
    name: String,
    min: u32,
    max: u32,
    sex: String,
    laps: Option<u32>,
    race_type: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct RaceConfig {
    pub category: Option<u16>,
    pub race_type: Option<u16>,
    pub race_type_name: Option<String>,
    pub number: Option<u16>,
    pub f_name: Option<u16>,
    pub l_name: Option<u16>,
    pub sex: Option<u16>,
    pub year: Option<u16>,
    pub title: Option<String>,
    pub categories: Option<Vec<Category>>,
    pub laps: Option<u16>,
    pub team: Option<u16>,
}

pub fn get_raceconfig(filename: &str) -> RaceConfig {
    let toml_data = match File::open(filename) {
        // The file is open (no error).
        Ok(mut file) => {
            let mut content = String::new();
            // Read all the file content into a variable (ignoring the result of the operation).
            file.read_to_string(&mut content).unwrap();
            content
        }
        Err(error) => {
            println!("Error opening file {}: {}", filename, error);
            String::from("")
        }
    };
    // dbg!("{}", &toml_data);
    toml::from_str(toml_data.as_str()).unwrap()
}

//<'a>(x: &'a str, y: &'a str) -> &'a str {
pub fn join_results_registration<'a>(
    results: &'a Vec<ResultEntry>,
    registration: &HashMap<String, Rc<Racer>>,
) -> Vec<(Rc<&'a ResultEntry>, Option<Rc<Racer>>)> {
    let default_racer = Racer {
        number: "999".to_string(),
        f_name: "Default".to_string(),
        l_name: "Racer".to_string(),
        race_type: "standard".to_string(),
        category: "XXX".to_string(),
        year: "1900".to_string(),
        sex: "yes".to_string(),
        laps: 1,
        team: "the bests".to_string(),
    };
    let rc_racer = Rc::new(default_racer);

    let mut results_w_reg = Vec::new();
    for result in results {
        let number = &result.number;
        match number {
            Some(number) => {
                let racer = match registration.get(number) {
                    Some(racer) => Rc::clone(racer),
                    None => Rc::clone(&rc_racer),
                };
                results_w_reg.push((Rc::new(result), Some(racer)));
            }
            None => {
                results_w_reg.push((Rc::new(result), None));
            }
        };
    }
    results_w_reg
}

pub fn make_registration_with_results<'a>(
    results: &'a Vec<ResultEntry>,
    registration: &HashMap<String, Rc<Racer>>,
) -> Vec<RaceResult> {
    results
        .iter()
        .map(|resultentry| {
            let number = &resultentry.number;

            let secs = &resultentry.time.as_secs();
            let h: u64 = secs / 3600;
            let m: u64 = (secs - (h * 3600)) / 60;
            let s: u64 = secs - (h * 3600) - (m * 60);
            let time = format!("{:0>2}:{:0>2}:{:0>2}", h, m, s);

            let mut result = RaceResult {
                time,
                number: "".to_string(),
                f_name: "".to_string(),
                l_name: "".to_string(),
                year: "".to_string(),
                sex: "".to_string(),
                race_type: "".to_string(),
                category: "".to_string(),
                lap: 1,
                team: "".to_string(),
            };

            match number {
                Some(number) => {
                    result.number = number.to_string();
                    if let Some(racer) = registration.get(number) {
                        result.f_name = racer.f_name.to_string();
                        result.l_name = racer.l_name.to_string();
                        result.year = racer.year.to_string();
                        result.sex = racer.sex.to_string();
                        result.race_type = racer.race_type.to_string();
                        result.category = racer.category.to_string();
                        result.lap = racer.laps;
                        result.team = racer.team.to_string();
                    };
                }
                None => (),
            };

            result
        })
        .collect::<Vec<RaceResult>>()
}

pub fn get_categories(registration: &registration::Registration) -> HashSet<String> {
    registration
        .iter()
        .map(|(_, racer)| racer.category.to_string())
        .collect::<HashSet<String>>()
}

pub fn get_category_results(category: &str, raceresults: &Vec<RaceResult>) -> Vec<RaceResult> {
    let mut cat_laps: HashMap<String, u32> = HashMap::new();

    raceresults
        .iter()
        .filter_map(|raceresult| {
            if raceresult.category == category {
                // dbg!(&raceresult.race_type);
                let lap = *cat_laps.get(&raceresult.number).unwrap_or(&0);
                cat_laps.insert(String::from(&raceresult.number), lap + 1);
                if lap + 1 == raceresult.lap {
                    return Some(RaceResult {
                        time: raceresult.time.to_string(),
                        number: raceresult.number.to_string(),
                        f_name: raceresult.f_name.to_string(),
                        l_name: raceresult.l_name.to_string(),
                        year: raceresult.year.to_string(),
                        sex: raceresult.sex.to_string(),
                        race_type: raceresult.race_type.to_string(),
                        category: raceresult.category.to_string(),
                        lap: lap + 1,
                        team: raceresult.team.to_string(),
                    });
                }
            }
            None
        })
        .collect::<Vec<RaceResult>>()
}

pub fn save_results_as_csv(raceresults: &Vec<RaceResult>, filename: &str) -> std::io::Result<()> {
    let mut file = File::create(filename)?;

    //Poradi;Startovni cislo;Cas;Jmeno;Prijmeni;Typ;Kategorie

    raceresults.iter().fold(1, |rank: u32, raceresult| {
        writeln!(
            &mut file,
            "{};{};{};{};{};{};{}",
            rank,
            raceresult.number,
            raceresult.time,
            raceresult.f_name,
            raceresult.l_name,
            raceresult.race_type,
            raceresult.category
        )
        .or_else(|_| Err(()))
        .unwrap();
        rank + 1
    });
    Ok(())
}

pub fn create_results_html_table(raceresults: &Vec<RaceResult>) -> String {
    let mut html_table = String::from(
        "<table border=\"1\" cellpadding=\"5\"><tr><td>Poradi</td><td>Startovni Cislo</td><td>Cas</td><td>Jmeno</td><td>Team</td></tr>\n"
    );
    let mut rank = 1;
    for raceresult in raceresults {
        let name;
        if raceresult.l_name != "" {
            name = format!("{} {}", raceresult.f_name, raceresult.l_name);
        } else {
            name = String::from(&raceresult.f_name);
        }

        let table_line = format!(
            "<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>\n",
            rank, raceresult.number, raceresult.time, name, raceresult.team
        );
        html_table.push_str(table_line.as_str());
        rank = rank + 1;
    }
    html_table.push_str("</table>");
    html_table
}

pub fn create_category_nav_bar(categories: &HashSet<String>) -> String {
    let mut nav_bar = String::from(
        "<div id=\"navbar\">
<a href=\"#all\">Celkove</a>",
    );
    for c in categories {
        let c_ref = format!(" -- <a href=\"#{}\">{}</a>", c, c);
        nav_bar.push_str(c_ref.as_str());
    }
    nav_bar.push_str("</div>");
    nav_bar
}

pub fn save_results_as_html(results: &Vec<ResultEntry>) -> std::io::Result<()> {
    let raceconfig = get_raceconfig("race.toml");
    let title = raceconfig.title.unwrap_or("Blblabla".to_string());
    let mut registration = registration::Registration::new();
    for path in glob("registrace*.csv").unwrap().filter_map(Result::ok) {
        // let new_registration = registration::read_registration(path.to_str().unwrap()).unwrap();
        registration::append_to_registration(path.to_str().unwrap(), &mut registration).unwrap();
    }
    let raceresults = make_registration_with_results(&results, &registration);
    let categories = get_categories(&registration);
    let mut file = File::create("index.html")?;

    let header = r#"<html><meta charset="utf-8" http-equiv="refresh" content="120"><style>
* {
  box-sizing: border-box;
}
#navbar {
       position:fixed;
       top: 0;
       margin:auto;
       left: 0;
       right: 0;
       width: 100%;
    height: 20px;
    background-color: #ffffff;
      }
</style><body>
"#;
    writeln!(&mut file, "{}", header)
        .or_else(|_| Err(()))
        .unwrap();

    let navbar = create_category_nav_bar(&categories);
    writeln!(&mut file, "{}\n", navbar)
        .or_else(|_| Err(()))
        .unwrap();

    writeln!(&mut file, "<div id=\"header\"><h1>{}", title)
        .or_else(|_| Err(()))
        .unwrap();

    for category in categories {
        let mut html = format!("<div id=\"{}\"><h3>{}</h3>\n", category, category);
        writeln!(&mut file, "{}", html)
            .or_else(|_| Err(()))
            .unwrap();

        let cat_results = get_category_results(category.as_str(), &raceresults);
        html = create_results_html_table(&cat_results);
        writeln!(&mut file, "{}", html)
            .or_else(|_| Err(()))
            .unwrap();
    }

    let html = format!(
        "<div id=\"all\"><h3>Celkove</h3>\n{}",
        create_results_html_table(&raceresults)
    );
    writeln!(&mut file, "{}", html)
        .or_else(|_| Err(()))
        .unwrap();

    Ok(())
}

pub fn save_result_categories_as_csv(results: &Vec<ResultEntry>) -> std::io::Result<()> {
    let mut registration = registration::Registration::new();
    for path in glob("registrace*.csv").unwrap().filter_map(Result::ok) {
        registration::append_to_registration(path.to_str().unwrap(), &mut registration).unwrap();
    }
    let raceresults = make_registration_with_results(&results, &registration);
    let categories = get_categories(&registration);
    for category in categories {
        let mut file = File::create(format!("vysledky-{}.csv", category))?;
        let cat_results = get_category_results(category.as_str(), &raceresults);
        cat_results.iter().fold(1, |rank: u32, raceresult| {
            if raceresult.l_name != "" {
                writeln!(
                    &mut file,
                    "{};{};{};{};{};{};{}",
                    rank,
                    raceresult.number,
                    raceresult.l_name,
                    raceresult.f_name,
                    raceresult.year,
                    raceresult.sex,
                    raceresult.time,
                )
                    .or_else(|_| Err(()))
                    .unwrap();
            } else {
                writeln!(
                    &mut file,
                    "{};{};{};{};{};{}",
                    raceresult.number,
                    raceresult.f_name,
                    raceresult.year,
                    raceresult.team,
                    raceresult.l_name,
                    rank,
                )
                    .or_else(|_| Err(()))
                    .unwrap();
            }
            rank + 1
        });
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::race::Race;
    use super::*;

    #[test]
    fn test_save_results() -> Result<(), std::io::Error> {
        let mut race = Race::new();
        race.restore("examples/race.csv").unwrap_or_default();
        save_results_as_html(&race.results).expect("save_results_as_html failed");
        Ok(())
    }
}
