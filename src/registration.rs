use std::fs::File;
use std::io::prelude::*;

use std::collections::HashMap;
use std::rc::Rc;

use glob::glob;

use crate::{get_raceconfig};

#[derive(Debug)]
pub struct Racer {
    // Kod;Startovní číslo;Jméno;Příjmení;Pohlaví;Prezdívka;Rok narození;Tričko;Firma;Slevový kód
    pub number: String,
    pub f_name: String,
    pub l_name: String,
    pub category: String,
    pub race_type: String,
    pub year: String,
    pub sex: String,
    pub laps: u32,
    pub team: String,

}

pub type Registration = HashMap<String, Rc<Racer>>;

pub fn load_registration(registration: &mut Registration) -> std::io::Result<()> {
    for path in glob("registrace*.csv").unwrap().filter_map(Result::ok) {
        append_to_registration(path.to_str().unwrap(), registration).unwrap();
    }
    Ok(())
}

pub fn append_to_registration(
    filename: &str,
    registration: &mut Registration,
) -> std::io::Result<()> {
    let raceconfig = get_raceconfig("race.toml");
    let mut registration_data = String::new();
    let mut file = File::open(filename)?;
    file.read_to_string(&mut registration_data)?;
    let registration_data_lines = registration_data.as_str().lines();

    let race_type_name = raceconfig.race_type_name.unwrap_or(String::from(""));
    for registration_data_line in registration_data_lines {
        if registration_data_line.as_bytes()[0] == b'#' {
            // dbg!(registration_data_line);
            continue;
        }
        let fields: Vec<_> = registration_data_line.split(';').collect();

        let next_index = raceconfig.category.unwrap_or(1);
        let mut category = match next_index {
            0 => String::from("---"),
            _ => String::from(fields[next_index as usize - 1])
        };

        let next_index = raceconfig.number.unwrap_or(2);
        let number: String = String::from(fields[next_index as usize - 1]);

        if !number.is_empty() {
            let next_index = raceconfig.f_name.unwrap_or(3);
            let f_name: String = String::from(fields[next_index as usize - 1]);

            let next_index = raceconfig.l_name.unwrap_or(4);
            let mut l_name: String = String::from(fields[next_index as usize - 1]);
            if l_name == f_name {
                l_name = String::new();
            }

            let next_index = raceconfig.race_type.unwrap_or(3);
            let race_type = match next_index {
                0 => String::from(&race_type_name),
                _ => String::from(fields[next_index as usize - 1])
            };

            let next_index = raceconfig.sex.unwrap_or(5);
            let sex: String = String::from(fields[next_index as usize - 1]);

            let next_index = raceconfig.year.unwrap_or(7);
            let year: String = String::from(fields[next_index as usize - 1]);

            let mut laps = 0;

            if let Some(categories) = &raceconfig.categories {
                for c in categories {
                    // dbg!(&year);
                    if let Some(c_race_type) = c.race_type.as_ref() {
                        if c_race_type != &race_type {
                            continue;
                        }
                    }
                    if c.min <= year.parse().unwrap_or(0)
                        && c.max >= year.parse().unwrap_or(0)
                        && c.sex == sex
                    {
                        category = format!("{} - {}", race_type, c.name);
                        // dbg!(c.laps);
                        if let Some(c_laps) = c.laps {
                            laps = c_laps;
                        }
                    }
                }
            }

            if laps == 0 {
                if let Some(configlaps) = &raceconfig.laps {
                    laps = fields[*configlaps as usize - 1].parse::<u32>().unwrap();
                } else {
                    laps = 1;
                }
            }
            let mut team = String::new();
            if let Some(configteam) = &raceconfig.team {
                team = String::from(fields[*configteam as usize - 1]);
            }

            let racer = Racer {
                number: number.to_string(),
                f_name,
                l_name,
                race_type,
                category,
                year,
                sex,
                laps,
                team,
            };
            // dbg!(&racer);
            registration.insert(number, Rc::new(racer));
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_print_racer() {
        println!(
            "{:?}",
            Racer {
                number: String::from("1"),
                f_name: "Jiri".to_string(),
                l_name: "Kolace".to_string(),
                race_type: "standard".to_string(),
                category: "M".to_string(),
                year: "1990".to_string(),
                sex: "yes".to_string(),
            }
        );
    }
}
